<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>

<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"class="modal-title" id="exampleModalScrollableTitle"> ADD NEW FOOD ITEM</h6>
      </div>
        <div class="modal-body">
                <form action="code.php" method="POST" enctype="multipart/form-data">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label> Food Title: </label>
                            <input type="text" name="item_title" class="form-control" placeholder="Enter Food title" required="required">

                        </div>
                        <div class="form-group col-md-6">
                            <label> Food Category:</label>
                            <select name="item_cat" class="form-control" required="required" placeholder="Enter Food title" >
                                        <option>
                                        <!-- Select a Category -->
                                        </option>
                                        <?php
                                            require 'dbconfig.php';
                                            $get_cats = "select * from food_category where cat_type = 'food'";
                                            $run_cats = mysqli_query($connection, $get_cats);
                                            while($row_cats = mysqli_fetch_array($run_cats)){
                                                $cat_id = $row_cats['fcat_id'];
                                                $cat_title = $row_cats['food_cat'];
                                                echo "<option value='$cat_id'>$cat_title</option>";
                                            } 
                                        ?>
                                    </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Food Company/Hotel</label>
                            <select name="item_company" class="form-control" required="required">
                                        <option>
                                        <!-- Select a Hotel -->
                                        </option>
                                        <?php
                                            require 'dbconfig.php';
                                            $get_brand = "select * from food_company where company_type = 'food'";
                                            $run_brand = mysqli_query($connection, $get_brand);
                                            while($row_brand = mysqli_fetch_array($run_brand)){
                                                $brand_id = $row_brand['fcompany_id'];
                                                $brand_title = $row_brand['Company_name'];
                                                echo "<option value='$brand_id'>$brand_title</option>";
                                            } 
                                        ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Food Item Quantity:</label>
                            <input type="number" name="item_quantity" class="form-control" placeholder="Enter item quantity" required="required" min="0">
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Price</label>
                            <input type="text" name="item_price" class="form-control" required="required">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Discount:</label>
                            <select name="item_discounted_percentage" class="form-control" required="required" >
                                        <option>
                                        <!-- Choose in Percentage -->
                                        </option>
                                        <option>0</option>
                                        <option>10</option>
                                        <option>20</option>
                                        <option>40</option>
                                        <option>60</option>
                                    </select>
                        
                        </div>
                    </div>
                
                        
                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Food Image</label>
                        <input type="file" class="form-control" name="item_img">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Food Youtube Link</label>
                        <input type="text"  class="form-control" name="item_youtubelink" size="50" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                        <label for="inputZip">Food-Description</label>
                        <textarea name="item_desc" class="form-control" cols="20" rows="5"></textarea>
                        </div>
                    </div>
					
                    <div class="form-inline">
                                  <div class="form-group col-xs-3">
                        <div id="item_variants">
                                          <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#addon-modal">Add Item Choices</button>
                                      </div>                               
                      </div>
                    </div>
                        
                    <div style=" border-top: 0 none;" class="modal-footer"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit"  class="btn btn-primary"  name="addfooditem">Save</button>
                    </div>
                </form> 
        </div>
    </div>
  </div>
</div>

<!-- Food Variants -->
<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="modal fade" id="addon-modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1>Add New Item Choice</h1>
                            </div>

                            <div class="modal-body">
                                <form action="code.php" method="POST" enctype="multipart/form-data">

                                    <div class="form-row">    
                                        <div class="form-group col-md-9">
                                            <input type="text" class="form-control" placeholder="Title of Choices">
                                        </div>
										
                                        <div class="form-group col-md-3">
                                          <input type="button" class="btn btn-secondary" value="Add Choice" onclick="addChoice();">
                                        </div>
                                    </div>

                                    
                                    <div id="sub_Choice">
                                    
                                    </div>
                              
                              
                                    <div class="form-row" style="margin-top:30px;">
                                            <div class="form-group col-md-6">
                                                <input type="checkbox" id="addon-checkbox">
                                                <label>Required</label>
                                            </div>
                                    </div>

                                </form>
                            </div>                         
                            

                            <div class="modal-footer">


                              <input class="btn btn-default" value="Save">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- MODAL -->


<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">FoodItem's Data
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add New Food-Item
            </button>
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM food_items where item_type = 'food'";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Image </th>
            <th> ID </th>
            <th> Category </th>
            <th> Food Store </th>
            <th>Date Added</th>
            <th>Title</th>
            <th>Price </th>
            <th>Description </th>
            <th>Youtube link </th>
            <!-- <th>Discount </th> -->
            <th>Item Quantity </th>
			<th>EDIT </th>
            <th>DELETE </th>
          </tr>
        </thead>
        <tbody>

<?php 
  if(mysqli_num_rows($query_run) > 0)        
  {
      while($row = mysqli_fetch_assoc($query_run))
      {

        ?>

        




      <tr>
          <td> <?php echo '<img src="fooditem_images/'.$row['item_img'].'" width="100px;" height="100px;" alt="image" >' ?> </td>
          <td> <?php  echo $row['item_id']; ?></td>
          

          
          
          <!-- Fetch Category -->
          <?php 

        
          ?>

          <td> 
            <?php
           
              $category_id = $row['fcat_id']; 

              $get_cat_data = "Select * from food_category where fcat_id = '$category_id'";
              $run_cat_data = mysqli_query($connection,$get_cat_data);
              $fetch_cat_data = mysqli_fetch_array($run_cat_data);

              $category_name = $fetch_cat_data['food_cat'];

              echo $category_name;
            
            
            ?>
          </td>
          <td> 
            <?php  
                
              $hotel_id  = $row['fcompany_id'];

              $get_hotel_data = "Select * from food_company where fcompany_id = '$hotel_id'";
              $run_hotel_data = mysqli_query($connection,$get_hotel_data);
              $fetch_hotel_data = mysqli_fetch_array($run_hotel_data);

              $hotel_name = $fetch_hotel_data['Company_name'];

              echo "$hotel_name";
          
            ?>
          </td>
          <td> <?php  echo $row['date']; ?> </td>
          <td> <?php  echo $row['item_title']; ?></td>
          <td> RM <?php  echo $row['item_price'];  ?> </td>
          <!-- <td> <?php  echo $row['item_discounted_price']; ?>% </td> -->
          <td> <?php  echo $row['item_desc']; ?></td>
          <td> <?php  echo $row['youtube_link']; ?></td>

          <td> <?php  echo $row['item_quantity']; ?></td>
		  <td>
                        <form action="food_edit.php" method="post">
                            <input type="hidden" name="item_edit_id" value="<?php  echo $row['item_id']; ?>">
                            <button  type="submit" name="item_edit_btn" class="btn btn-success"> EDIT</button>
                        </form>
                  </td>
                  <td>
                      <form action="code.php" method="post">
                        <input type="hidden" name="deleteitem_id" value="<?php  echo $row['item_id']; ?>">
                        <button type="submit" name="deleteitem_btn" class="btn btn-danger"> DELETE</button>
                      </form>
                  </td>
      </tr>
<?php
      }
    }else {
      echo "No Record Found";
    }
?>



</tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>

<!--variants script-->
<script>
var counter = 0;

function addChoice()
{
  counter++;
  var sub_choice = document.createElement('div'); //Create a new <div>
  sub_choice.id = "sub_Choice" + counter; // Give the new <div> an id. <div id="sub_Choice1>
  sub_choice.innerHTML = "<div class='form-row'>" + "<div class='form-group col-md-5'>" + "<input type='text' class='form-control' placeholder='Choices'>" + "</div>" + "<div class='form-group col-md-2'>" + "<input type='text' class='form-control' placeholder='Prices'>" + "</div>" + "<div class='form-group col-md-1'>" + "<input type='button' class='btn btn-secondary' value='x' onClick=\"removeChoice('sub_Choice"+counter+"');\">"; + "</div>"  + "</div>";
  document.getElementById("sub_Choice").appendChild(sub_choice);
}

function removeChoice(id)
{
  var sub_elem = document.getElementById(id);
  return sub_elem.parentNode.removeChild(sub_elem);
}

</script>


