<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="container-fluid">

<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> EDIT FOOD CATEGORY </h6>
  </div>
  <div class="card-body">
<?php

    

    if(isset($_POST['editfoodcat_btn'])){
        $id = $_POST['editfoodcat_id'];
        
        require 'dbconfig.php'; 

        $query = "SELECT * FROM food_category WHERE fcat_id='$id'";
        $query_run = mysqli_query($connection, $query);

        foreach($query_run as $row)
        {
            ?>

                <form action="code.php" method="POST">

                    <div class="modal-body">

                        <input type="hidden" name="foodcat_updateid" value="<?php echo $row['fcat_id'] ?>" >

                        <div class="form-group">
                            <label>Food Category Name </label>
                            <input type="text" name="foodcat_updatename" value="<?php echo $row['food_cat'];?>" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-danger" name="cancelhotelbtn" data-dismiss="modal">CANCEL</button> -->
                        <button type="submit" name="updatefoodcatbtn" class="btn btn-primary">UPDATE</button>
                    </div>
                </form>
        <?php
        }

      
    }
?>
  </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>