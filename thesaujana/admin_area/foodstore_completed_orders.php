<?php
include('foodstore_security.php');
include('includes/header.php'); 
include('includes/navbar_food.php'); 
?>

<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h4 class="m-0 font-weight-bold text-primary">Completed Orders
    </h4>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

      <?php

      // : Getting FoodStore ID
      require 'dbconfig.php';

      $store_email = $_SESSION['foodusername'];
                                    
      $get_foodstore_details = "Select * from food_company where company_email='$store_email'";
      $run_foodstore_details = mysqli_query($connection, $get_foodstore_details);
      $fetch_foodstore_details = mysqli_fetch_array($run_foodstore_details);
                                                                                                
      $foodstore_id = $fetch_foodstore_details['fcompany_id']; //:: Customer id 
      $foodstore_name = $fetch_foodstore_details['Company_name']; //:: customer name

      //:: Getting the food_items..
      $query = "SELECT * FROM completed_orders where hotel_id = '$foodstore_id' ";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Invoice No </th>
            <th> Customer Details </th>
            <th> Customer Order No </th>
            <th>Order Date</th>
            <th>Order Description</th>
            <th>Amount </th>
            <th>Pickup Order Date & Time </th>
            <th>Order Status </th>
            <th>Order type </th>
          </tr>
        </thead>
        <tbody>

<?php 
  if(mysqli_num_rows($query_run) > 0)        
  {
      while($row = mysqli_fetch_assoc($query_run))
      {

        ?>

        




        <tr>
          <?php 

          ?>
          <td> <?php  echo $row['invoice_no']; ?></td>
          <td> <?php  

            //: Getting customer data: 

                $id_customer =  $row['customer_id']; 
                                                    
                $get_customer_details = "Select * from customers where customer_id='$id_customer'";
                $run_customer_details = mysqli_query($connection, $get_customer_details);
                $fetch_customer_details = mysqli_fetch_array($run_customer_details);
                                                                                                                
                $customer_id = $fetch_customer_details['customer_id']; //:: Customer id 
                $customer_name = $fetch_customer_details['customer_name'];
                $customer_contactno = $fetch_customer_details['customer_contact']; 
                $customer_email = $fetch_customer_details['customer_email'];//:: customer name

                echo " ID: $customer_id<br>";
                echo " Name: $customer_name<br>";
                echo " Contact No: $customer_contactno<br>";
                echo " Email: $customer_email<br>";

          
          
          
          ?></td>
          <td> <?php  echo $row['customer_order_no']; ?></td>
          <td> <?php  echo $row['order_date']; ?></td>
          <td> <?php  echo $row['order_description']; ?></td>
          <td> <?php  echo $row['due_amount']; ?>RM</td>

          <td> <?php  echo $row['pickup_order_date']; ?>
                <?php  echo "<br>" ?>
                <?php  echo $row['pickup_order_time']; ?></td>

            <td> <?php  echo $row['order_status']; ?></td>
            <td> <?php  echo $row['order_type']; ?></td>
        </tr>
<?php
      }
    }else {
      echo "No Record Found";
    }
?>



</tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>