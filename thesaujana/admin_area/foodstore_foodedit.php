<?php
include('foodstore_security.php');
include('includes/header.php'); 
include('includes/navbar_food.php'); 
?>


<div class="container-fluid">

<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> EDIT Hotel Data </h6>
  </div>
  <div class="card-body">
<?php

    

    if(isset($_POST['editfoodstore_btn'])){
        $id = $_POST['editfoodstore_id'];
        
        require 'dbconfig.php'; 

        $query = "SELECT * FROM food_items WHERE item_id='$id'";
        $query_run = mysqli_query($connection, $query);

        foreach($query_run as $row)
        {
            ?>

            <form action="foodstore_code.php" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="food_updateid" value="<?php echo $row['item_id'] ?>" >

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label> Food Title: </label>
                    <input type="text" name="food_updatetitle" value="<?php echo $row['item_title'] ?>" class="form-control" placeholder="Enter Food title" required="required">

                </div>
                <div class="form-group col-md-6">
                    <label> Food Category (       
                                <?php $foodcategory_id = $row['fcat_id'];

                                $get_cat_name = "select * from food_category where fcat_id = '$foodcategory_id'";
                                $run_cat_name = mysqli_query($connection, $get_cat_name);
                                $fetch_cat_name = mysqli_fetch_array($run_cat_name);
                                $cat_name = $fetch_cat_name['food_cat'];
                                echo "$cat_name";
                                
                                ?>
                          )</label>
                    
                    <select name="food_updatecat" class="form-control" required="required" placeholder="Enter Food title" >
                                <option>
                              
                                </option>
                                <?php
                                    require 'dbconfig.php';
                                    $get_cats = "select * from food_category where cat_type = 'food'";
                                    $run_cats = mysqli_query($connection, $get_cats);
                                    while($row_cats = mysqli_fetch_array($run_cats)){
                                        $cat_id = $row_cats['fcat_id'];
                                        $cat_title = $row_cats['food_cat'];
                                        echo "<option value='$cat_id'>$cat_title</option>";
                                    } 
                                ?>
                            </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                            <label>Food Item Quantity:</label>
                            <input type="number" name="item_quantity" class="form-control" placeholder="Enter item quantity" required="required" min="0">
                        </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Price</label>
                    <input type="text" name="food_updateprice" value="<?php echo $row['item_price'] ?>" class="form-control" required="required" />
                </div>
                <!-- <div class="form-group col-md-6"> -->
                    <!-- <label>Discount:</label> -->
                    <!-- <select name="item_discounted_percentage" class="form-control" required="required" >
                                <option>
                                </option>
                                <option>10</option>
                                <option>20</option>
                                <option>40</option>
                                <option>60</option>
                                <option>NO</option>
                            </select> -->
                
                <!-- </div> -->
            </div>

                
            <div class="form-row">
                <div class="form-group col-md-12">
                <label for="inputZip">Food Image</label>
                <input type="file" class="form-control" name="food_updateimg">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                <label for="inputZip">Food Youtube Link</label>
                <input type="text"  class="form-control" value="<?php echo $row['youtube_link'] ?>" name="food_updateyoutubelink" size="50"/>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                <label for="inputZip">Food-Description</label>
                <textarea name="food_updatedesc" class="form-control" cols="20" rows="5"><?php echo $row['item_desc']?></textarea>
                </div>
            </div>
              
			<div class="form-inline">
                        <div class="form-group col-xs-3">
							<div id="item_variants">
                                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#addon-modal">Add Item Choices</button>
                            </div>                               
						</div>
			</div>			  
				
            <div style=" border-top: 0 none;" class="modal-footer"> 
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit"  class="btn btn-primary"  name="updatefoodstoreitem">Save</button>
            </div>
            </form> 
        <?php
        }

      
    }
?>
  </div>
  </div>
</div>

</div>
<!-- Food Variants -->
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="modal fade" id="addon-modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1>Add New Item Choice</h1>
                            </div>

                            <div class="modal-body">
                                <form action="code.php" method="POST" enctype="multipart/form-data">

                                    <div class="form-row">    
                                        <div class="form-group col-md-9">
                                            <input type="text" class="form-control" placeholder="Title of Choices">
                                        </div>
										
                                        <div class="form-group col-md-3">
                                          <input type="button" class="btn btn-secondary" value="Add Choice" onclick="addChoice();">
                                        </div>
                                    </div>

                                    
                                    <div id="sub_Choice">
                                    
                                    </div>
                              
                              
                                    <div class="form-row" style="margin-top:30px;">
                                            <div class="form-group col-md-6">
                                                <input type="checkbox" id="addon-checkbox">
                                                <label>Required</label>
                                            </div>
                                    </div>

                                </form>
                            </div>                         
                            

                            <div class="modal-footer">


                              <input class="btn btn-default" value="Save">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->


<?php
include('includes/scripts.php');
include('includes/footer.php');
?>

<!--variants script-->
<script>
var counter = 0;

function addChoice()
{
  counter++;
  var sub_choice = document.createElement('div'); //Create a new <div>
  sub_choice.id = "sub_Choice" + counter; // Give the new <div> an id. <div id="sub_Choice1>
  sub_choice.innerHTML = "<div class='form-row'>" + "<div class='form-group col-md-5'>" + "<input type='text' class='form-control' placeholder='Choices'>" + "</div>" + "<div class='form-group col-md-2'>" + "<input type='text' class='form-control' placeholder='Prices'>" + "</div>" + "<div class='form-group col-md-1'>" + "<input type='button' class='btn btn-secondary' value='x' onClick=\"removeChoice('sub_Choice"+counter+"');\">"; + "</div>"  + "</div>";
  document.getElementById("sub_Choice").appendChild(sub_choice);
}

function removeChoice(id)
{
  var sub_elem = document.getElementById(id);
  return sub_elem.parentNode.removeChild(sub_elem);
}

</script>