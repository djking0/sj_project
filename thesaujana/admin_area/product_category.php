<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>


<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 style="text-align: center;  color: black; font-weight: bold; font-size: 16px;"class="modal-title" id="exampleModalScrollableTitle"> ADD NEW FOOD ITEM</h6>
      </div>
        <div class="modal-body">
                <form action="code.php" method="POST" enctype="multipart/form-data">

                      <div class="form-group">
                        <label> Product Category Name: </label>
                          <input type="text" name="productcategory_name" class="form-control" placeholder="Enter Food Category Name" required="required">
                      </div>
                   
                      <div class="form-group">
                        <label for="inputZip">Product Image</label>
                        <input type="file" class="form-control" name="productcat_img" required="required">
                      </div>
                        
                    <div style=" border-top: 0 none;" class="modal-footer"> 
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit"  class="btn btn-primary"  name="addproductcategorybtn">Save</button>
                    </div>
                </form> 
        </div>
    </div>
  </div>
</div>


<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Product Category's Data
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
              Add New Product Category
            </button>
    </h6>
  </div>

  <div class="card-body">

    <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">

      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM food_category where cat_type = 'product'";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Image </th>
            <th> ID </th>
            <th> Name </th>
            <th>EDIT </th>
            <th>DELETE </th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {

                  ?>

                <tr>
                <td> <?php echo '<img src="homepage_images/'.$row['cat_image'].'" width="100px;" height="100px;" alt="image" >' ?> </td>
                    <td> <?php  echo $row['fcat_id']; ?></td>
                    <td> <?php  echo $row['food_cat']; ?></td>
                    
                    <td>
                        <form action="productcategory_edit.php" method="post">
                            <input type="hidden" name="editproductcat_id" value="<?php  echo $row['fcat_id']; ?>">
                            <button  type="submit" name="editproductcat_btn" class="btn btn-success"> EDIT</button>
                        </form>
                  </td>
                  <td>
                      <form action="code.php" method="post">
                        <input type="hidden" name="deleteproductcat_id" value="<?php  echo $row['fcat_id']; ?>">
                        <button type="submit" name="deleteproductcat_btn" class="btn btn-danger"> DELETE</button>
                      </form>
                  </td>
                </tr>
          <?php
                }
              }else {
                echo "No Record Found";
              }
          ?>
     
          
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>