<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>




<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">User's Questions or Queries
    </h6>
  </div>

  <div class="card-body">

    <div class="table-responsive">
      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM contact_query";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> User Name </th>
            <th> User Email </th>
            <th>User Query</th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {

                  ?>

                <tr>
                   
                    <td> <?php  echo $row['contact_customer_name']; ?></td>
                    <td> <?php  echo $row['contact_customer_email']; ?></td>
                    <td> <?php  echo $row['contact_customer_enquiry']; ?> </td>
                </tr>
          <?php
                }
              }else {
                echo "No Record Found";
              }
          ?>
     
          
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>