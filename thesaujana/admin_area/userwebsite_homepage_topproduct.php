<?php
include('security.php');
include('includes/header.php'); 
include('includes/navbar.php'); 
?>



<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Top-Products Section
    </h6>
  </div>

  <div class="card-body">

  <?php
      if(isset($_SESSION['success']) && $_SESSION['success']!=''){
        echo '<h2> '.$_SESSION['success'].' </h2>.';
        unset($_SESSION['success']);
      } 
      if(isset($_SESSION['status']) && $_SESSION['status']!=''){
        echo '<h2> '.$_SESSION['status'].' </h2>.';
        unset($_SESSION['status']);
      } 
      
    ?>

    <div class="table-responsive">
      <?php

      
      require 'dbconfig.php';

      $query = "SELECT * FROM food_items where item_type = 'product'";
      $query_run = mysqli_query($connection, $query);

      ?>

      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Image </th>
            <th> ID </th>
            <th> Name </th>
            <th> Store Name </th>
            <th> Price </th>
            <th> Display At Top Product </th>
            <th> Trending Type</th>
          </tr>
        </thead>
        <tbody>

          <?php 
            if(mysqli_num_rows($query_run) > 0)        
            {
                while($row = mysqli_fetch_assoc($query_run))
                {

                  ?>

                <tr>
                    <td><?php echo '<img src="fooditem_images/'.$row['item_img'].'" width="100px;" height="100px;" alt="image" >' ?> </td>
                    <td> <?php  echo $row['item_id']; ?></td>
                    <td> <?php  echo $row['item_title']; ?></td>
                    <td> <?php  echo $row['fcompany_id']; ?> </td>
                    <td> <?php  echo $row['item_price']; ?>RM</td>
                    
                    <td>
                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php 

                                $top_product = $row['top_product'];

                                if($top_product == "yes"){
                                    echo "Yes";
                                }else {
                                    echo "No";
                                }
                            ?>
                            </button>

                            <?php

                            $top_product_id = $row['top_product'];
                            // echo "$top_product_id";
                            
                            if($top_product_id == "yes"){

                                ?>

                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                                <form action="code.php" method="post">
                                <input type="hidden" name="top_product_no_id" value="<?php  echo $row['item_id']; ?>">
                                <button  type="submit" name="top_product_no_btn"  class="dropdown-item">NO</button>
                                </form>
                            
                               
                                </div>

                                <?php

                              
                            }else {

                                ?>

                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">


                                <form action="code.php" method="post">
                                    <input type="hidden" name="top_product_yes_id" value="<?php  echo $row['item_id']; ?>">
                                    <button  type="submit" name="top_product_yes_btn" class="dropdown-item"> YES</button>
                                </form>

                                </div>

                                <?php

                            }
                
                            
                            ?>
                            
                        </div>

                     </td>

                     <td>

                     <div class="btn-group" role="group">
                      <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          
                      
                      <?php  
                      
                      $trending_id = $row['trending'];

                      if($trending_id == ''){
                        echo "NULL";
                      }else {
                        echo $row['trending'];
                      }
                      
                      ?>
                      </button>
                      <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

                          <form action="code.php" method="post">
                          <input type="hidden" name="new_arrival_id" value="<?php  echo $row['item_id']; ?>">
                          <button  type="submit" name="new_arrival_btn"  class="dropdown-item">New Arrival</button>
                          </form>

                          <form action="code.php" method="post">
                          <input type="hidden" name="trending_search_id" value="<?php  echo $row['item_id']; ?>">
                          <button  type="submit" name="trending_search_btn"  class="dropdown-item">Trending Search</button>
                          </form>

                          <form action="code.php" method="post">
                          <input type="hidden" name="daily_discover_id" value="<?php  echo $row['item_id']; ?>">
                          <button  type="submit" name="daily_discover_btn"  class="dropdown-item">Daily Discover</button>
                          </form>

                          <form action="code.php" method="post">
                          <input type="hidden" name="null_id" value="<?php echo $row['item_id']; ?>">
                          <button  type="submit" name="null_btn"  class="dropdown-item">Null</button>
                          </form>
                      
                        
                      </div>
                  </div>
                    
                     </td>

                     

                </tr>
          <?php
                }
              }else {
                echo "No Record Found";
              }
          ?>
     
          
        
        </tbody>
      </table>

    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>