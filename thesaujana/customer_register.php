<?php
session_start();
include("includes/db.php");
include("functions/functions.php");


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <!-- <title>The Saujana Store</title> -->

    <!-- Bootstrap CDN -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />

    <!--  Font Awesome CDN -->
    <script src="https://kit.fontawesome.com/23412c6a8d.js"></script>

    <!-- Slick Slider -->
    <link
      rel="stylesheet"
      type="text/css"
      href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
    />

    
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="./css/viewproduct.css" />

  </head>

  <body>
    <!-- header -->

    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12 col-12">
            <div class="btn-group">
              <button
                class="btn border dropdown-toggle my-md-4 my-2 text-white"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                ENG
              </button>
              <div class="dropdown-menu">
                <a href="#" class="dropdown-item">Bhasa - Malaysia</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-12 text-center">
            <h2 class="my-md-3 site-title text-white">The Saujana </h2>
          </div>
          <div class="col-md-4 col-12 text-right">
          <p class="my-md-4 header-links">
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"";
                }else{
                  
                  echo"<a href='' class='px-1'> $_SESSION[customer_email] </a>";
                  echo"<a class='px-2' style='color:white;'>|</a>";
                }
              ?>
              <?php
                if(!isset($_SESSION['customer_email'])){
                echo"<a href='checkout.php' class='px-2'>login</a>";
                echo"<a class='px-2' style='color:white;'>|</a>";
                echo"<a href='register.php' class='px-2'>Create an Account</a>";
                }else{
                  echo"<a href='logout.php' class='px-2'>logout</a>";
                }
              ?>
            </p>
          </div>
        </div>
      </div>

      <div class="container-fluid p-0">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a href="index.php" class="nav-link" >HOME <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > FEATURES </a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > COLLECTION</a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > SHOP</a>
              </li>
              <li class="nav-item">
                <a href="index.php" class="nav-link" > ABOUT US</a>
              </li>
              <li class="nav-item">
                <a href="cart.php" class="nav-link" > Your Cart <b class="badge badge-pill badge-light float-right"> <?php total_items();?></b></a>
              </li>

              <?php
                        $ip = getIp();

                  ?>

                  <?php
                          cart();
                  ?>
            </ul>
          </div>
          <div class="navbar-nav">
            <form class="form-inline my-2 my-lg-0" method="get" action="results.php" enctype="multipart/form-data">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search a Product" aria-label="Search" name="user_query"/>
                  <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                  </form>
          </div>
        </nav>
      </div>

        <!-- BreadCrumbs -->
        <div class="container-fluid p-0">
          <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <ul class="breadcrumbs">
              <li class="breadcrumbs__item">
                <a href="#" class="breadcrumbs__link">Home</a>
              </li>
              <li class="breadcrumbs__item">
                <a href="#" class="breadcrumbs__link breadcrumbs__link--active"> Checkout <br></a>
              </li>
          </nav>
            </ul>
        </div>
      <!-- /BreadCrumbs -->

    </header>

    <!-- /header -->

    <!-- Main Section   -->

    <!-- ============================ COMPONENT REGISTER   ================================= -->
	<div class="card mx-auto" style="max-width:520px; margin-top:40px;">
      <article class="card-body">
		<headera class="mb-4"><h4 class="card-title">Sign up</h4></headera>
		<form action="register.php" method="post" enctype="multipart/form-data">
				<div class="form-row">
					<div class="col form-group">
						<label>first name</label>
					  	<input type="text" class="form-control" name="c_fname" placeholder="" required>
					</div> <!-- form-group end.// -->
					<div class="col form-group">
						<label>Last name</label>
					  	<input type="text" class="form-control" placeholder="" name="c_lname" required>
					</div> <!-- form-group end.// -->
				</div> <!-- form-row end.// -->
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" placeholder="" name="c_email" required>
					<small class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div> <!-- form-group end.// -->
                <div class="form-group">
					<label>Contact Number</label>
					<input type="text" class="form-control" placeholder="" name="c_number" required>
				</div>
                <div class="form-group">
					<label>Your Address</label>
					<input type="text" class="form-control" placeholder="" name="c_address" required>
				</div>
                <div class="form-group">
					<label>Profile Image</label>
					<input type="file" class="form-control" placeholder="" name="c_image" required>
				</div>
				<div class="form-group">
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" checked="" type="radio" name="c_gender" value="option1">
					  <span class="custom-control-label"> Male </span>
					</label>
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" type="radio" name="c_gender" value="option2">
					  <span class="custom-control-label"> Female </span>
					</label>
				</div> <!-- form-group end.// -->
				<div class="form-row">
					<div class="form-group col-md-6">
					  <label>City</label>
					  <input type="text" class="form-control" name="c_city" required>
					</div> <!-- form-group end.// -->
					<div class="form-group col-md-6">
					  <label>Country</label>
					  <select id="inputState" class="form-control" name="c_country" required>
					    <option> Choose...</option>
					      <option>Singapore</option>
					      <option selected="">Malaysia</option>
					  </select>
					</div> <!-- form-group end.// -->
				</div> <!-- form-row.// -->
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Create password</label>
					    <input class="form-control" type="password" name="c_pass" required>
					</div> <!-- form-group end.// --> 
					<div class="form-group col-md-6">
						<label>Repeat password</label>
					    <input class="form-control" type="password">
					</div> <!-- form-group end.// -->  
				</div>
			    <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block" name="register"> Register  </button>
			    </div> <!-- form-group// -->      
			    <div class="form-group"> 
		            <label class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" checked=""> <div class="custom-control-label"> I am agree with <a href="#">terms and contitions</a>  </div> </label>
		        </div> <!-- form-group end.// -->           
			</form>
		</article><!-- card-body.// -->
    </div> <!-- card .// -->
    <p class="text-center mt-4">Have an account? <a href="">Log In</a></p>
    <br><br>
<!-- ============================ COMPONENT REGISTER  END.// ================================= -->

    <!-- /Main Section   -->

   
    <hr> </hr>
    <!-- Footer -->

    <footer>
        <div class="container-fluid px-5">
          <div class="row">
            <div class="col-md-4 col-sm-12">
              <h4>Contact Us</h4>
              <div class="row pl-md-1 text-secondary">
                <div class="col-12">
                  <i class="fa fa-home px-md-2"></i>
                  <small>S0-7-01, Menara 1, No 3, Jalan Bangsar, KL Eco City</small>
                </div>
              </div>
              <div class="row pl-md-1 text-secondary py-4">
                  <div class="col-12">
                      <i class="fa fa-paper-plane px-md-2"></i>
                      <small>www.gogoempire.com</small>
                    </div>
              </div>
               <div class="row pl-md-1 text-secondary">
                  <div class="col-12">
                      <i class="fa fa-phone-volume px-md-2"></i>
                      <small>(+60) 123817908</small>
                    </div>
               </div>

              <div class="row social text-secondary">
                
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Our Services</h4>
              <div class="d-flex flex-column pl-md-3">
                <small class="pt-0">Kuala Lumpur</small>
                <small>Selangor</small>
                <small>KL Central</small>
              </div>
            </div>
            <div class="col-md-2 col-sm-12">
              <h4>Extras</h4>
              <div class="d-flex flex-column pl-md-3">
                  <small class="pt-0">About GogoEmpire</small>
                  <small>Collection</small>
                  <small>Contact Us</small>
                </div>
            </div>
            <div class="col-md-4 follow-us col-sm-12">
              <h4>Follow Instagram</h4>
              <div class="d-flex flex-row">
                <img src="./assets/256_n.jpg" alt="Instagram 1" class="img-fluid">
                <img src="./assets/792_n.jpg" alt="Instagram 2" class="img-fluid">
                <img src="./assets/392_n.jpg" alt="Instagram 3" class="img-fluid">
              </div>
              <div class="d-flex flex-row">
                  <img src="./assets/664_n.jpg" alt="Instagram 1" class="img-fluid">
                  <img src="./assets/088_n.jpg" alt="Instagram 2" class="img-fluid">
                  <img src="./assets/896_n.jpg" alt="Instagram 3" class="img-fluid">
                </div>
            </div>
          </div>
        </div>

        <div class="container-fluid news pt-5">
          <div class="row">
            <!-- <div class="col-md-6 col-12 pl-5">
              <h4 class="primary-color font-roboto m-0 p-0">
                Need Help? Call Our Award-Warning
              </h4>
              <p class="m-0 p-0 text-secondary">
                Support Team 24/7 At (+60) 176844365
              </p>
            </div>
            <div class="col-md-4 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <input type="text" class="form-control border-0 bg-light" placeholder="Enter Your email Address">
            </div>
            <div class="col-md-2 col-12 my-md-0 my-3 pl-md-0 pl-5">
              <button class="btn bg-primary-color text-white">Subscribe</button>
            </div>
          </div>
        </div>

        <div class="container text-center">
          <p class="pt-5">
            <img src="./assets/payment.png" alt="payment image" class="img-fluid">
          </p>
          <small class="text-secondary py-4">© COPYRIGHT 2019 SAUJANA HOTEL SDN BHD (123333-T)</small>
        </div>

    </footer>

    <!-- /Footer -->


    <script
      src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script
      type="text/javascript"
      src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
    ></script>
    <script src="./js/main.js"></script>
  </body>



<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="dashboard.js"></script></body>
  
</html>

<?php 

if(isset($_POST['register'])){

    $c_fname = $_POST['c_fname'];
    $c_lname = $_POST['c_lname'];
    $c_name = "$c_fname $c_lname";

    $c_email = $_POST['c_email'];
    $c_pass = $_POST['c_pass'];
    $c_country = $_POST['c_country'];
    $c_city = $_POST['c_city'];
    $c_contact = $_POST['c_number'];
    $c_address = $_POST['c_address'];
    $c_image = $_FILES['c_image']['name'];
    $c_image_tmp = $_FILES['c_image']['tmp_name'];
    
    $c_ip = getIp();
    // echo "$c_name", "$c_email", "$c_pass", "$c_country", "$c_city", "$c_contact", "$c_address", "$c_image" ;


    $insert_customer ="insert into customers (customer_name,customer_email,customer_pass,customer_country,customer_city,customer_contact,customer_address,customer_image,customer_ip) values ('$c_name','$c_email','$c_pass','$c_country',
    '$c_city','$c_contact','$c_address','$c_image','$c_ip')";

    

    

    $run_customer = mysqli_query($con, $insert_customer);

    $sel_cart = "select * from cart where ip_add='$c_ip'";

    $run_cart = mysqli_query($con, $sel_cart); 
    
    $check_cart = mysqli_num_rows($run_cart);

    if($check_cart>0){

        $_SESSION['customer_email']=$c_email; 

        echo "<script>alert('Account has been created successfully, Thanks!')</script>";
        echo "<script>window.open('checkout.php','_self')</script>";
    }else {
       $_SESSION['customer_email']=$c_email;
        echo "<script>alert('Account has been created successfully, Thanks!')</script>";
        echo "<script>window.open('index.php','_self')</script>";
    }

}



?>